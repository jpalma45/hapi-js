const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/testDB', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => console.log('database is connected'))
    .catch(err => console.log(err))